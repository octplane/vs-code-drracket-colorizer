## Purpose ##

This is the most basic syntax colorizer and brackets matcher for DrRacket 
language.

## Example ##

![Example](https://bytebucket.org/Dima4ka/vs-code-drracket-colorizer/raw/3e8e4cc46e7995ea9563e7b45005862fd21ba42f/Example.png)

## Source ##

[BitBucket](https://bitbucket.org/Dima4ka/vs-code-drracket-colorizer)

## Contribution ##

You are very welcome to fork & pull request this extension.

Special appreciation to [@soegaard](https://github.com/soegaard) for 
**.tmLanguage** file.

## License ##

[MIT](https://bitbucket.org/Dima4ka/vs-code-drracket-colorizer/raw/3e8e4cc46e7995ea9563e7b45005862fd21ba42f/LICENSE)
